<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {

    Route::resource('/sewa', 'SewaController');

    Route::resource('/mobil', 'MobilController');

    Route::resource('/booking', 'BookingController');

    Route::resource('/profile', 'ProfileController')->only(['index', 'store', 'update']);

    Route::get('/mobil/create', 'MobilController@create');

    Route::get('/mobil', 'MobilController@index');

    Route::post('/mobil', 'MobilController@store');

    Route::get('/mobil/{mobil_id}', 'MobilController@show');

    Route::get('/mobil/{mobil_id}/edit', 'MobilController@edit');

    Route::put('/mobil/{mobil_id}', 'MobilController@update');

    Route::delete('/mobil/{mobil_id}', 'MobilController@destroy');


    Route::get('/sewa/create', 'SewaController@create');

    Route::get('/sewa', 'SewaController@index');

    Route::post('/sewa', 'SewaController@store');

    Route::get('/sewa/{sewa_id}', 'SewaController@show');

    Route::get('/sewa/{sewa_id}/edit', 'SewaController@edit');

    Route::put('/sewa/{sewa_id}', 'SewaController@update');

    Route::delete('/sewa/{sewa_id}', 'SewaController@destroy');

    Route::get('/sewa-excel', 'SewaController@export');

    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('/booking/create', 'BookingController@create');

    Route::get('/booking', 'BookingController@index');

    Route::post('/booking', 'BookingController@store');

    Route::get('/booking/{booking_id}', 'BookingController@show');

    Route::get('/booking/{booking_id}/edit', 'BookingController@edit');

    Route::put('/booking/{booking_id}', 'BookingController@update');

    Route::delete('/booking/{booking_id}', 'BookingController@destroy');

    Route::get('/booking-excel', 'BookingController@export');

});

Auth::routes();

Route::get('/test-dompdf', function() {
    
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/test-dompdf-2', 'PdfController@test');

Route::get('/test-excel', 'UsersController@export');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});