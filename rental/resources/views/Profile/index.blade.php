@extends('rental.master')

@section('title')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile User {{ auth::user()->user}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')
    @if($profile !== null)
    <div class="card card-primary ml-2 mr-2">
        <div class="card-header">
          <h3 class="card-title">Profile {{ $profile->id }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/profile/{{ $profile->id }}" method="POST" enctype="multipart/form-data" >
        @csrf
          <div class="card-body">
            <p>Nama : {{ $profile->user->name}}</p>
            <p>Email : {{ $profile->user->email}}</p>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="alamat" class="form-control" name="alamat" id="alamat" value="{{ old('alamat', $profile->alamat) }}" placeholder="Enter Alamat">
                @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nomor_hp">Nomor Handphone</label>
                <input type="nomor_hp" class="form-control" name="nomor_hp" id="nomor_hp" value="{{ old('nomor_hp', $profile->nomor_hp) }}" placeholder="Enter Nomor">
                @error('nomor_hp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
              <label for="umur">Umur</label>
              <input type="umur" class="form-control" name="umur" id="umur" value="{{ old('umur', $profile->umur) }}" placeholder="Enter Age">
              @error('umur')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
              @enderror
            </div>
            <div class="form-group">
              <label for="bio">Biodata</label>
              <input type="bio" class="form-control" name="bio" id="bio" value="{{ old('bio', $profile->bio) }}" placeholder="Enter Bio">
              @error('bio')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update Profile</button>
          </div>
        </form>
      </div>

    @else

    <div class="card card-primary ml-2 mr-2">
        <div class="card-header">
          <h3 class="card-title">Create New Profile</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/profile" method="POST" enctype="multipart/form-data" >
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="nama" class="form-control" name="nama" id="nama" value="{{ old('nama', '') }}" placeholder="Enter Nama">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="alamat" class="form-control" name="alamat" id="alamat" value="{{ old('alamat', '') }}" placeholder="Enter Alamat">
                @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nomor_hp">Nomor Handphone</label>
                <input type="nomor_hp" class="form-control" name="nomor_hp" id="nomor_hp" value="{{ old('nomor_hp', '') }}" placeholder="Enter Nomor">
                @error('nomor_hp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="umur" class="form-control" name="umur" id="umur" value="{{ old('umur', '') }}" placeholder="Enter Age">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                </div>
            <div class="form-group">
                <label for="bio">Biodata</label>
                <input type="bio" class="form-control" name="bio" id="bio" value="{{ old('bio','') }}" placeholder="Enter Bio">
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">UPDATE</button>
        </div>
        </form>
    </div>
    
    @endif

@endsection