<div class="">
  <div class="main-menu-header">
      <img class="img-80 img-radius" src="{{ asset('/images/foto.png')}}" alt="User-Profile-Image">
      <div class="user-details">
          <span id="more-details">{{ auth::user()->name }}</span>
      </div>
  </div>
</div>
<div class="pcoded-navigation-label">Pages</div>
<div class="pcoded-item pcoded-left-item">
  <li class="pcoded-hasmenu ">
      <a href="javascript:void(0)" class="waves-effect waves-dark">
          <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
          <span class="pcoded-mtext">Car</span>
          <span class="pcoded-mcaret"></span>
      </a>
      <ul class="pcoded-submenu">
          <li class="">
              <a href="/mobil" class="waves-effect waves-dark">
                  <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                  <span class="pcoded-mtext">List Car</span>
                  <span class="pcoded-mcaret"></span>
              </a>
          </li>
          <li class="">
              <a href="/mobil/create" class="waves-effect waves-dark">
                  <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                  <span class="pcoded-mtext">Create New Car</span>
                  <span class="pcoded-mcaret"></span>
              </a>
          </li>
      </ul>
  </li>
  <li class="pcoded-hasmenu ">
    <a href="javascript:void(0)" class="waves-effect waves-dark">
        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
        <span class="pcoded-mtext">Rent</span>
        <span class="pcoded-mcaret"></span>
    </a>
    <ul class="pcoded-submenu">
        <li class="">
            <a href="/sewa" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                <span class="pcoded-mtext">List Customers</span>
                <span class="pcoded-mcaret"></span>
            </a>
        </li>
        <li class="">
            <a href="/sewa/create" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                <span class="pcoded-mtext">Create New Customers</span>
                <span class="pcoded-mcaret"></span>
            </a>
        </li>
    </ul>
  </li>
  <li class="pcoded-hasmenu ">
    <a href="javascript:void(0)" class="waves-effect waves-dark">
        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
        <span class="pcoded-mtext">Booking</span>
        <span class="pcoded-mcaret"></span>
    </a>
    <ul class="pcoded-submenu">
        <li class="">
            <a href="/booking" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                <span class="pcoded-mtext">List Booking</span>
                <span class="pcoded-mcaret"></span>
            </a>
        </li>
        <li class="">
            <a href="/booking/create" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                <span class="pcoded-mtext">Create New Cust Booking</span>
                <span class="pcoded-mcaret"></span>
            </a>
        </li>
    </ul>
  </li>
  <li class="pcoded-hasmenu ">
    <a href="javascript:void(0)" class="waves-effect waves-dark">
        <span class="pcoded-micon"><i class="ti-id-badge"></i><b>A</b></span>
        <span class="pcoded-mtext">Profile</span>
        <span class="pcoded-mcaret"></span>
    </a>
    <ul class="pcoded-submenu">
        <li class="">
            <a href="/profile" class="waves-effect waves-dark">
                <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                <span class="pcoded-mtext">Profile</span>
                <span class="pcoded-mcaret"></span>
            </a>
        </li>
        <li class="">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i> {{ __('Logout') }}</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
        </li>
    </ul>
</li>
</ul>
</div>