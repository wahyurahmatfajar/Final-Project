@extends('rental.master')

@section('title')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Car</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@endsection

@section('content')

<div class="card card-primary ml-2 mr-2">
  <div class="card-header">
    <h3 class="card-title">Edit Booking : {{ $booking->id }}</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="/booking/{{ $booking->id }}" method="POST" enctype="multipart/form-data" >
  @csrf
  @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="nama" class="form-control" name="nama" id="nama" value="{{ old('nama', $booking->nama) }}" placeholder="Enter Name Car">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        </div>
        <div class="form-group">
          <label for="tgl_booking">Tanggal Booking</label>
          <input type="date" class="form-control" name="tgl_booking" id="tgl_booking" value="{{ old('tgl_booking', $booking->tgl_booking) }}">
          @error('tgl_booking')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
          @enderror
        </div>
      <div class="form-group">
        <label for="tgl_sewa">Tanggal Sewa</label>
        <input type="date" class="form-control" name="tgl_sewa" id="tgl_sewa" value="{{ old('tgl_sewa', $booking->tgl_sewa) }}" placeholder="Enter Price">
        @error('tgl_sewa')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">UPDATE</button>
    </div>
  </form>
</div>
@endsection