<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'umur' => 'required',
            'bio' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required',
    	]);
 
        Profile::create([
    		'umur' => $request->umur,
            'bio' => $request->bio,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'nomor_hp' => $request->nomor_hp,
            'user_id' => Auth::id()
    	]);
 
    	return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'umur' => 'required',
            'bio' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required'
    	]);

        $profile = Profile::find($id);
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->nama = $request->nama;
        $profile->alamat = $request->alamat;
        $profile->nomor_hp = $request->nomor_hp;
        $profile->update();
        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
