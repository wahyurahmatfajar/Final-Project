<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function test(){
        $data = "Bismillah..";
        $pdf = PDF::loadView('pdf.index', compact('data'));
        return $pdf->download('index.pdf');
    }
}
