<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Exports\BookingExport;
use Maatwebsite\Excel\Facades\Excel;

class BookingController extends Controller
{   
    public function export() 
    {
        return Excel::download(new BookingExport, 'databooking.xlsx');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $booking = Booking::all();
        return view('rental.booking.index', compact('booking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rental.booking.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required|unique:mobil',
            'tgl_booking' => 'required',
    		'tgl_sewa' => 'required',
    	]);

        Booking::create([
    		'nama' => $request->nama,
            'tgl_booking' => $request->tgl_booking,
    		'tgl_sewa' => $request->tgl_sewa,
            'users_id' => Auth::id()
    	]);
        
        Alert::success('success', 'Booking Berhasil Disimpan!');
    	return redirect('/booking');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::find($id);
        return view('rental.booking.edit', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::findorfail($id);
        return view('rental.booking.edit', compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required|unique:mobil',
            'tgl_booking' => 'required',
    		'tgl_sewa' => 'required',
    	]);

        $booking = Booking::find($id);
        $booking->nama = $request->nama;
        $booking->tgl_booking = $request->tgl_booking;
        $booking->tgl_sewa = $request->tgl_sewa;
        $booking->update();

        Alert::success('success', 'Data Booking Berhasil DiUpdate!');
        return redirect('/booking');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $booking->delete();

        Alert::success('success', 'Data Booking Berhasil DiDelete!');
        return redirect('/booking');
    }
}
