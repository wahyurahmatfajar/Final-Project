<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sewa;
use App\Mobil;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Exports\SewaExport;
use Maatwebsite\Excel\Facades\Excel;


class SewaController extends Controller
{   
    public function export() 
    {
        return Excel::download(new SewaExport, 'datacustomer.xlsx');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobil = Mobil::all();
        $sewa = Sewa::all();
        return view('rental.sewa.index', compact('sewa', 'mobil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mobil = Mobil::all();
        $sewa = Sewa::all();
        return view('rental.sewa.create', compact('mobil', 'sewa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required|unique:mobil',
            'alamat' => 'required',
            'no_hp' => 'required',
            'lama_sewa' => 'required',
            'mobil_id' => 'required'
    	]);
 
        Sewa::create([
    		'nama' => $request->nama,
            'alamat' => $request->alamat,
            'no_hp' => $request->no_hp,
            'lama_sewa' => $request->lama_sewa,
            'mobil_id' => $request->mobil_id,
            'users_id' => Auth::id()
    	]);
        
        Alert::success('success', 'Berhasil Menambah Data Customer');

    	return redirect('/sewa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $mobil = Mobil::all();
        $sewa = Sewa::findorfail($id);
        return view('rental.sewa.show', compact('sewa', 'mobil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = Mobil::all();
        $sewa = Sewa::findorfail($id);
        return view('rental.sewa.edit', compact('sewa', 'mobil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required|unique:mobil',
            'alamat' => 'required',
            'no_hp' => 'required',
            'lama_sewa' => 'required',
            'mobil_id' => 'required'
    	]);

        $sewa = Sewa::find($id);
        $sewa->nama = $request->nama;
        $sewa->alamat = $request->alamat;
        $sewa->no_hp = $request->no_hp;
        $sewa->lama_sewa = $request->lama_sewa;
        $sewa->mobil_id = $request->mobil_id;
        $sewa->update();

        Alert::success('success', 'Data Customer Berhasil DiUpdate!');
        return redirect('/sewa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sewa = Sewa::find($id);
        $sewa->delete();

        Alert::success('success', 'Data Customer Berhasil DiDelete!');
        return redirect('/sewa');
    }
}
