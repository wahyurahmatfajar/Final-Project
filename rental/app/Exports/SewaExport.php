<?php

namespace App\Exports;

use App\Sewa;
use Maatwebsite\Excel\Concerns\FromCollection;

class SewaExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Sewa::all();
    }
}
