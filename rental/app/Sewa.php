<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sewa extends Model
{
    protected $table = "sewa";
    protected $guarded = [];


    public function mobil() {
        return $this->belongsTo('App\Mobil');
    }
}
